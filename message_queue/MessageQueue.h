#ifndef MESSAGE_QUEUE_
#define MESSAGE_QUEUE_

#include <thread>
#include <mutex>
#include <condition_variable>
#include <deque>

template <class T>
class MessageQueue {
    public:
    T dequeue() {
        std::unique_lock<std::mutex> mlock(mutex_);

        while (queue_.empty()) {
            cond_.wait(mlock);
        }
        auto val = queue_.front();
        queue_.pop_front();

        return val;
    }

    void enqueue(T item) {
        std::unique_lock<std::mutex> mlock(mutex_);
        queue_.push_back(item);
        mlock.unlock();
        cond_.notify_one();
    }

    void remove_by_what(int w) {
        std::unique_lock<std::mutex> mlock(mutex_);
        for (auto it = queue_.begin(); it != queue_.end(); ) {
            if (it->what == w) {
                it = queue_.erase(it);
            }
            ++it;
        }
        mlock.unlock();
        cond_.notify_one();
    }

    private:
    std::deque<T> queue_;
    std::mutex mutex_;
    std::condition_variable cond_;
};

#endif
