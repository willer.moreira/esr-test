#include "Message.h"
#include "MessageQueue.h"
#include <iostream>
#include <sstream>
#include <thread>

const int numberOfItems = 20;

void coutSafe(std::string x) {
    static std::mutex mutex;
    std::unique_lock<std::mutex> locker(mutex);
    std::cout << x << "\n";
}

void producer(MessageQueue<Message>& q) {
    for (int i = 1; i <= numberOfItems; ++i) {
        std::ostringstream toPrint;
        toPrint << "producer enqueued: " << i%2;
        coutSafe(toPrint.str());

        q.enqueue(Message(i%2));
    }
}

void consumer(MessageQueue<Message>& q) {
    for (int i = 1; i <= numberOfItems/2; ++i) {
        auto item = q.dequeue();
        std::ostringstream toPrint;
        toPrint << "consumer dequeued: " << item.what;
        coutSafe(toPrint.str());
    }
}

int main() {
    MessageQueue<Message> q;

    std::thread p(std::bind(producer, std::ref(q)));
    p.join();

    std::cout << "\nremoving all occurrences of 1\n\n";
    q.remove_by_what(1);

    std::thread c(std::bind(consumer, std::ref(q)));
    c.join();

  return 0;
}
