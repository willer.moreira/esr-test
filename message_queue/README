Message queue

Implement a message queue class with thread-safe operations to enqueue and dequeue messages. The message queue should be used in a classic concurrent producer-consumer problem, which is also the test scenario you should provide. The messages that are stored by the message queue should look like this:
 
class Message {
	int what;
	int arg1;
	int arg2;
	void* obj; // C++
	Object obj; // Java
}

Furthermore the message queue should support operations to get the first element of the queue and also to delete all elements with a specific “what” field from the queue. Choose an adequate data structure to implement the message queue and tell us the runtime of each class method using the O calculus. Use either C/C++ (POSIX, Win32, WinRT) or Java APIs to implement the program.

== COMPILE
    - Run: make

== RUNNING
    - ./test_queue

    - The program will enqueue 20 items, 10 with what == 0 and 10 with what == 1,
      after that the function remove_by_what(int w) is executed to remove all items with what == 1,
      then the ramaining items (what == 0) are dequeued.

    - I used the std::deque to implement this queue, since I needed to remove elements from any position.
    
