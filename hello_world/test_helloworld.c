#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "CUnit/Basic.h"
#include "helloworld.h"

static FILE* stream = NULL;

int init_suite1(void) {
    FILE* outfile;
    int stdout_copy;
    pthread_t t1, t2;

    outfile = fopen("stdout.txt", "w");
    stdout_copy = dup(fileno(stdout));

    dup2(fileno(outfile), fileno(stdout));

    fclose(outfile);

    pthread_create(&t1, NULL, &print_hello, NULL);
    pthread_create(&t2, NULL, &print_world, NULL);

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);

    dup2(stdout_copy, fileno(stdout));
    close(stdout_copy);

    stream = fopen("stdout.txt", "r");
    if (stream == NULL)
        return -1;

    return 0;
}

int clean_suite1(void)
{
   if (0 != fclose(stream)) {
      return -1;
   }
   else {
      stream = NULL;
      return 0;
   }
}

void test_helloworld(void)
{
    char *line = NULL;
    size_t len = 0;
    ssize_t read;

    while ((read = getline(&line, &len, stream)) != -1) {
        CU_ASSERT(0 == strcmp(line, "Hello World!\n"));
    }

    free(line);
}

int main()
{
    CU_pSuite pSuite = NULL;

    if (CUE_SUCCESS != CU_initialize_registry())
        return CU_get_error();

    pSuite = CU_add_suite("Suite_1", init_suite1, clean_suite1);
    if (NULL == pSuite) {
        CU_cleanup_registry();
        return CU_get_error();
    }

    if (NULL == CU_add_test(pSuite, "test of helloworld", test_helloworld))
    {
        CU_cleanup_registry();
        return CU_get_error();
    }

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();

    return CU_get_error();
}
