#include <pthread.h>
#include "helloworld.h"

int main(int argc, char *argv[]) {
    pthread_t t1, t2;
    pthread_create(&t1, NULL, &print_hello, NULL);
    pthread_create(&t2, NULL, &print_world, NULL);

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);

    return 0;
}

