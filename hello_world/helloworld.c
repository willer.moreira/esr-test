#include <stdio.h>
#include <pthread.h>
#include "helloworld.h"

pthread_mutex_t mutex;
pthread_cond_t condition;
int turn = 0; // 0: "Hello " 1: "World!"

#ifdef TEST_MODE
int count = 0;
int max_prints = 100000;
#endif

void *print_hello() {
    while (1) {
        pthread_mutex_lock(&mutex);

        if (turn) {
            pthread_cond_wait(&condition, &mutex);
        }

#ifdef TEST_MODE
        if (count >= max_prints) {
            return;
        }
#endif

        printf("Hello ");

        turn = !turn;
        pthread_cond_signal(&condition);
        pthread_mutex_unlock(&mutex);

    }
}

void *print_world() {
    while (1) {
        pthread_mutex_lock(&mutex);
        if (!turn) {
            pthread_cond_wait(&condition, &mutex);
        }

        printf("World!\n");

#ifdef TEST_MODE
        count ++;

        if (count >= max_prints) {
            pthread_cond_signal(&condition);
            pthread_mutex_unlock(&mutex);
            return;
        }
#endif

        turn = !turn;
        pthread_cond_signal(&condition);
        pthread_mutex_unlock(&mutex);


    }
}
